INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (9, 'Oriharukon Dagger', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (11, 'Crystal Dagger', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (12, '\\aHWind Blade Dagger', 8);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (13, 'Finger of Death', 1);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (14, 'Claw of Chaos', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (16, 'Blade of Revenge', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (49, 'Long Sword of Pretender King', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (50, 'Sword of Flames', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (51, 'Gold Scepter', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (54, 'Sword of Kurtz', 5);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (55, 'Blade of Darkness', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (56, 'Death Blade', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (57, 'Tsurugi', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (58, 'Fire Sword of Death Knight', 5);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (61, '\\aHExecutuion Sword of Fidelity', 10);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (62, 'Two-Handed Sword of Pretender King', 6);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (76, 'Edoryu of Ronde', 8);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (81, 'Edoryu of Bravery', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (84, 'Kaiser Edoryu', 4);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (86, '\\aHEdoryu of Red Shadow', 10);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (114, 'Symbol of Dignity', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (121, 'Staff of Ice Queen', 8);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (124, 'Staff of Baphomet', 5);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (126, 'Staff of Mana', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (127, 'Steel Staff of Mana', 1);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (131, 'Staff of Strength', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (134, '\\aHCrystalized Staff', 20);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (160, 'Claw of Savage King', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (162, 'Claw of Bravery', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (164, 'Kaiser Claw', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (166, 'Claw of Hate', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (177, 'Crossbow of Bravery', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (182, 'Ancient Bowgun', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (184, 'Bow of Flames', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (188, 'Lastabad Heavy Cross Bow', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (189, 'Kaiser Crossbow', 5);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (190, 'Sayhas Bow', 5);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (191, 'Bow of Cold Mastery', 5);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (196, 'Two-Handed Sword of Varlok', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (197, 'Two-Handed Sword of Varlok', 1);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (198, 'Two-Handed Sword of Varlok', 1);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (199, 'Two-Handed Sword of Varlok', 1);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (200, 'Two-Handed Sword of Varlok', 1);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (201, 'Two-Handed Sword of Varlok', 1);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (202, 'Two-Handed Sword of Varlok', 1);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (203, 'Two-Handed Sword of Varlok', 5);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (205, 'Longbow of Moon', 15);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (256, 'Pumpkin Sword', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (294, '\\aHMonarch\'s Thunder Sword', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (503, 'Sapphire Key Link', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (504, 'Dark Stone Key Link', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (506, 'Thebe Osiris Two-Handed Sword', 1);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (507, 'Thebe Osiris Edoryu', 1);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (508, 'Thebe Osiris Bow', 15);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (509, 'Thebe Osiris Staff', 1);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (600, 'Lighting Edge', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (601, 'Distraction', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (602, 'Mana Bazerado', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (603, 'Angel Staff', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (604, 'Freezing Lancer', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (605, 'Raging Wind', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (614, 'Kukurukan Spear', 1);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1010, 'Devil King\'s Two-Handed Sword', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1011, 'Devil King\'s Edoryu', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1012, 'Devil King\'s Staff', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1013, 'Devil King\'s Spear', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1014, 'Devil King\'s Bow', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1108, 'Hidden Devil\'s Chain Sword', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1109, 'Hidden Devil\'s Claw', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1110, 'Hidden Devil\'s Staff', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1111, 'Hidden Devil\'s Bow', 15);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1112, 'Hidden Devil\'s Key Link', 20);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1113, 'Hidden Devil\'s Sword', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1115, 'Sacred Sword', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1116, 'Sacred Staff', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1117, 'Sacred Claw', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1118, 'Sacred Long Bow', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1119, 'Extreme Chain Sword', 10);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1120, 'Cold Key Link', 20);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1121, '\\aFTwo-Hand Wword of Night Bardo', 7);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1123, 'Blood Chainsword', 10);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1124, 'Claw of Destruction', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1125, 'Dual Blade of Destruction', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1134, 'Meditation staff', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1135, 'Key Link of Resonance', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1136, 'Nightmare Longbow', 15);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1137, 'Claw of Anger', 5);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (1509, 'Thebes Osirisu Staff', 1);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (10011, 'Crystal Short Sword', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (10031, 'Staff of Force', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (11121, '\\aFTwo hand sword of Night Bardo', 10);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (11125, 'Dual blade of destruction', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (100009, 'Orichalcum Dagger', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (100049, 'General Sword', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (100057, 'Turgi', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (100062, 'Great Sword', 7);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (100084, 'Dark Edoryu', 4);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (100164, 'Dark Claw', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (100189, 'Dark Crossbow', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (100212, 'Sea God Trident', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (200172, 'Halloween Pumpkin Staff', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (202000, 'Sword of Command', 0);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (202002, 'Red Knight Great Sword', 3);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (202003, 'Zero\'s Staff', 20);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (202011, '\\aHGaia Rage', 50);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (202012, '\\aHHyperion\'s Despair', 120);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (202013, '\\aHFear of Kronos', 50);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (202014, '\\aHTitan\'s Fury', 50);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (203002, 'Red Knight Great Sword', 3);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (203004, 'Iron Axe', 3);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (203005, 'Raffian Axe', 4);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (203006, 'Tempest Axe', 5);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (203007, 'Giant Axe', 7);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (203011, 'Hidden Magic Axe', 5);
INSERT INTO `weapon_damege`(`item_id`, `name`, `addDamege`) VALUES (203017, '\\aFExtinct Chainsword', 20);
