INSERT INTO `castle`(`castle_id`, `name`, `war_time`, `tax_rate`, `public_money`) VALUES (1, 'Kent Castle', '2016-12-25 00:50:19', 10, 81);
INSERT INTO `castle`(`castle_id`, `name`, `war_time`, `tax_rate`, `public_money`) VALUES (2, 'Orc Forest', '2015-09-07 21:29:29', 10, 0);
INSERT INTO `castle`(`castle_id`, `name`, `war_time`, `tax_rate`, `public_money`) VALUES (3, 'Windawood Castle', '2008-10-18 17:00:00', 10, 676542);
INSERT INTO `castle`(`castle_id`, `name`, `war_time`, `tax_rate`, `public_money`) VALUES (4, 'Giran Castle', '2016-12-25 01:00:49', 10, 7493536);
INSERT INTO `castle`(`castle_id`, `name`, `war_time`, `tax_rate`, `public_money`) VALUES (5, 'Heine Castle', '2008-10-18 17:00:00', 10, 0);
INSERT INTO `castle`(`castle_id`, `name`, `war_time`, `tax_rate`, `public_money`) VALUES (6, 'Dwarf Castle', '2008-10-18 17:00:00', 10, 2565399);
INSERT INTO `castle`(`castle_id`, `name`, `war_time`, `tax_rate`, `public_money`) VALUES (7, 'Aden Castle', '2015-09-07 20:37:08', 10, 38501801);
INSERT INTO `castle`(`castle_id`, `name`, `war_time`, `tax_rate`, `public_money`) VALUES (8, 'Diad Fortress', '2008-10-18 17:00:00', 10, 0);
