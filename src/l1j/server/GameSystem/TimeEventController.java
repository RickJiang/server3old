package l1j.server.GameSystem;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;

import l1j.server.Config;
import l1j.server.server.model.L1World;
import l1j.server.server.serverpackets.S_PacketBox;

@SuppressWarnings("unused")
public class TimeEventController extends Thread {
    private static TimeEventController _instance;
    private boolean _TimeEventStart;
    private boolean _TimeEventOpen;
    private boolean _TimeEventTime;
    private int _TimeEventTing;
    private boolean Close;
    private static long sTime = 0L;
    private String NowTime = "";
    private static final int LOOP = 19; //starts at 7pm 
    private static final SimpleDateFormat s = new SimpleDateFormat("HH",
            Locale.KOREA);

    private static final SimpleDateFormat ss = new SimpleDateFormat(
            "MM-dd HH:mm", Locale.KOREA);

    private static Random _random = new Random(System.nanoTime());

    public boolean getTimeEventStart() {
        return _TimeEventStart;
    }

    public void setTimeEventStart(boolean timeevent) {
        _TimeEventStart = timeevent;
    }

    public boolean getTimeEventOpen() {
        return _TimeEventOpen;
    }

    public void setTimeEventOpen(boolean timeevent) {
        _TimeEventOpen = timeevent;
    }

    public boolean getTimeEventTime() {
        return _TimeEventTime;
    }

    public void setTimeEventTime(boolean timeevent) {
        _TimeEventTime = timeevent;
    }

    public int getTimeEventTing() {
        return _TimeEventTing;
    }

    public void setTimeEventTing(int i) {
        _TimeEventTing = i;
    }

    public static TimeEventController getInstance() {
        if (_instance == null) {
            _instance = new TimeEventController();
        }
        return _instance;
    }


    public void run() {
        try {
            while (true) {
                try {
                    Thread.sleep(10000L);
                } catch (Exception e) {
                }
                if (!isOpen())
                    continue;
                if (L1World.getInstance().getAllPlayers().size() <= 0)
                    continue;
                L1World.getInstance().broadcastServerMessage(
                        "\\fW[Event Manager]：Hello~! Event will start soon!!");
                try {
                    Thread.sleep(2000L);
                } catch (Exception e) {
                }
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Event Manager]：Players~ What kind of event you like?"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fW[Event Manager]：Drawing Events!!");
                try {
                    Thread.sleep(5000L);
                } catch (Exception e) {
                }
                setTimeEventTime(false);
                setTimeEventOpen(true);
                setTimeEventTime(true);
                setTimeEventStart(true);
                int i = TimeEventChoice();
                TimeEventGo(i);
                setTimeEventTing(i);
                int t = 0;
                while (t <= 10) {  //Proceed for 5 hours and 30 minutes and rinse 10 times
                    try {
                        Thread.sleep(1000 * 60 * 30L);
                    } catch (Exception e) {
                    }
                    TimeEventMent(i);
                    ++t;
                }
                L1World.getInstance().broadcastServerMessage(
                        "\\fW[Event Manager]In a few minutes the event will begin.");
                L1World.getInstance().broadcastServerMessage(
                        "\\fW[Event Manager]One of the events is over and rerun randomly.");
                try {
                    Thread.sleep(30000L);
                } catch (Exception e) {
                }
                TimeEventStop(i);
            }
        } catch (Exception e1) {
        }
    }


    public String OpenTime() {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(sTime);
        return ss.format(c.getTime());
    }


    private boolean isOpen() {
        NowTime = getTime();
        if ((Integer.parseInt(NowTime) % LOOP) == 0) //every night at 7pm
            return true;
        return false;
    }


    private String getTime() {
        return s.format(Calendar.getInstance().getTime());
    }


    private int TimeEventChoice() {
        int i = _random.nextInt(12);
        return i;
    }


//Event Time Start Message + Effect

    private void TimeEventGo(int i) {
        switch (i) {
            case 0:
                Config.RATE_XP *= 1.1;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[EXP Event]：Exp +10%"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Manager]The amount of experience gained increased by +10% from now.");
                break;
            case 1:
                Config.RATE_KARMA *= 1.3;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Friendship Event]：Friendly +30%"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Manager]The amount of friendship gained will be increased by +30%.");
                break;
            case 2:
                Config.RATE_XP *= 1.2;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[EXP Event]：Exp +20%"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Manager]The amount of experience gained from now increases by +20%.");
                break;
            case 3:
                Config.RATE_DROP_ADENA *= 1.1;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Adena Event]：Adena +10%"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Manager]Adena acquisition amount increased by +10%");
                break;
            case 4:
                Config.RATE_DROP_ADENA *= 1.2;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Adena Event]：Adena +20%"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Manager]Adena acquisition amount increase by +20%");
                break;
            case 5:
                Config.RATE_DROP_ADENA *= 1.3;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Adena Event]：Adena +30%"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Manager]Adena acquisition amount increase by +30%");
                break;
            case 6:
                Config.FEATHER_NUM *= 1.1;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Feather Event]：Feather +10%"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Manager]Increase in Feather payment by +10%");
                break;
            case 7:
                Config.ENCHANT_CHANCE_WEAPON *= 1.1;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Enchant Event]：Weapon Enchant +10%"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Manager]Weapon enchantment rate increase by +10%.");
                break;
            case 8:
                Config.FEATHER_NUM *= 1.2;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Feather Event]：Feather +20%"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Manager]Increase in feather payment by +20%.");
                break;
            case 9:
                Config.RATE_DROP_ITEMS *= 1.2;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Drop Event]：Drop +20%"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Manager]Increase drop rate by +20%.");
                break;
            case 10:
                Config.ENCHANT_CHANCE_ARMOR *= 1.1;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Enchant Event]：Armor Enchant +10%"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Manager]Armor enchantment rate increase by +10%.");
                break;
            case 11:
                Config.RATE_DROP_ITEMS *= 1.3;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Drop Event]：Drop +30%"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Manager]Increase drop rate by +30%.");
                break;
        }
    }


// Event Start Time + Effect

    private void TimeEventMent(int i) {
        switch (i) {
            case 0:
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[EXP Event：Exp +10%]"));
                break;
            case 1:
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Friendship Event：Friendly +30%]"));
                break;
            case 2:
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[EXP Event：Exp +20%]"));
                break;
            case 3:
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Adena Event：Adena +10%]"));
                break;
            case 4:
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Adena Event：Adena +20%]"));
                break;
            case 5:
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Adena Event：Adena +30%]"));
                break;
            case 6:
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Feather Event：Feather +10%]"));
                break;
            case 7:
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Enchantment Event：Weapon Enchant +10%]"));
                break;
            case 8:
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Feather Event：Feather +20%]"));
                break;
            case 9:
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Drop Event：Drop +20%]"));
                break;
            case 10:
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Enchant Event：Armor Enchant +10%]"));
                break;
            case 11:
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fC[Drop Event：Drop +30%]"));
                break;
        }
    }


//Event Time End + Effect

    private void TimeEventStop(int i) {
        switch (i) {
            case 0:
                Config.RATE_XP /= 1.1;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fCExperience increase effect disappears."));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Time]Experience increase effect disappers.");
                break;
            case 1:
                Config.RATE_KARMA /= 1.3;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fCThe effect of increasing friendship disappers."));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Time]Friendship increase effect disappears.");
                break;
            case 2:
                Config.RATE_XP /= 1.2;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fCExperience increase effect disappers"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Time]Experience increase effect disappers");
                break;
            case 3:
                Config.RATE_DROP_ADENA /= 1.1;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fCAdena drop rate increase effect disappears."));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Time]Adena drop rate increase effect disappears.");
                break;
            case 4:
                Config.RATE_DROP_ADENA /= 1.2;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fCAdena drop rate increase effect disappears."));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Time]Adena drop rate increase effect disappears.");
                break;
            case 5:
                Config.RATE_DROP_ADENA /= 1.3;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fCAdena drop rate increase effect disappears."));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Time]Adena drop rate increase effect disappears.");
                break;
            case 6:
                Config.FEATHER_NUM /= 1.1;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fCfeather payment effect disappears"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Time]Additional pyament effect disappears");
                break;
            case 7:
                Config.ENCHANT_CHANCE_WEAPON /= 1.1;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fCWeapon enchantment increase effect disappears"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Time]Weapon enchantment increase effect disappears");
                break;
            case 8:
                Config.FEATHER_NUM /= 1.2;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fCFeather offering benefit disappears"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Time]Feather offering benefit disappears");
                break;
            case 9:
                Config.RATE_DROP_ITEMS /= 1.2;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fCItem drop rate increase effect disappears"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Time]Item drop rate increase effect disappears。");
                break;
            case 10:
                Config.ENCHANT_CHANCE_ARMOR /= 1.1;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fCArmor enchantment increase effect disappears"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Time]Armor enchantment increase effect disappears");
                break;
            case 11:
                Config.RATE_DROP_ITEMS /= 1.3;
                L1World.getInstance().broadcastPacketToAll(
                        new S_PacketBox(S_PacketBox.GREEN_MESSAGE,
                                "\\fCItem drop rate increase effect disappears"));
                L1World.getInstance().broadcastServerMessage(
                        "\\fU[Event Time]Item drop rate increase effect disappears。");
                break;
        }
        setTimeEventStart(false);
        Close = false;
    }
}

// リアルタイム倍率変動終わり

