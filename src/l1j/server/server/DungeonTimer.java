package l1j.server.server;

import java.util.Calendar;

import l1j.server.server.Controller.DungeonQuitController;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_ChatPacket;
import l1j.server.server.serverpackets.S_SystemMessage;

public class DungeonTimer implements Runnable {

    private static DungeonTimer instance;

    public static final int SleepTime = 1 * 60 * 1000; //Check every minute

    public static DungeonTimer getInstance() {
        if (instance == null) {
            instance = new DungeonTimer();
        }
        return instance;
    }

    @Override
    public void run() {
        try {
            for (L1PcInstance use : L1World.getInstance().getAllPlayers()) {
                if (use == null || use.getNetConnection() == null || use.noPlayerCK || use.noPlayerck2) {
                    continue;
                } else {
                    try {
                        if (use.getMapId() >= 53 && use.getMapId() <= 56
                                || use.getMapId() >= 15403 && use.getMapId() <= 15404) { // giran
                            GiranTimeCheck(use);
                        }
                        if (use.getMapId() >= 78 && use.getMapId() <= 82) { //Oren
                            OrenTimeCheck(use);
                        }
                        if (use.getMapId() >= 30 && use.getMapId() <= 33
                                || use.getMapId() >= 35 && use.getMapId() <= 37
                                || use.getMapId() == 814) { //DVC
                            DrageonTimeCheck(use);
                        }
                        /*if (use.getMapId() >= 451 && use.getMapId() <= 456
                                || use.getMapId() >= 460 && use.getMapId() <= 466
								|| use.getMapId() >= 470 && use.getMapId() <= 478
								|| use.getMapId() >= 490 && use.getMapId() <= 496
								|| use.getMapId() >= 530 && use.getMapId() <= 534
								|| use.getMapId() == 479){
							RadungeonTimeCheck(use);
						}*/
                        if (use.getMapId() == 303) { // Monsom
                            SomeTimeCheck(use);
                        }
                        if (use.getMapId() == 430 || use.getMapId() == 400) { // Tomb of the Spirit, ancient tomb
                            SoulTimeCheck(use);
                        }
                        if (use.getMapId() == 280 || use.getMapId() == 281 || use.getMapId() == 282
                                || use.getMapId() == 283 || use.getMapId() == 284) { //Balrog Camp
                            newdodungeonTimeCheck(use);
                        }
                        if (use.getMapId() == 285 || use.getMapId() == 286 || use.getMapId() == 287 || use.getMapId() == 288
                                || use.getMapId() == 289) { //Yahi camp
                            OrenTimeCheck(use);
                        }
                        if (use.getMapId() == 5555 || use.getMapId() == 5556) { //Ordon PC
                            icedungeonTimeCheck(use);
                        }
                        if (use.getMapId() == 1 || use.getMapId() == 2) { //Roll
                            islanddungeonTimeCheck(use);
                        }
                        init();

                    } catch (Exception a) {
                        //not
                    }
                }
            }
        } catch (Exception a) {
            System.out.println("DungeonTimer error");
        }
    }

    private void init() {
        try {
            Calendar cal = Calendar.getInstance();
            int hour = Calendar.HOUR;
            int minute = Calendar.MINUTE;
            /** 0 Am , 1 Pm * */
            String ampm = "PM";
            if (cal.get(Calendar.AM_PM) == 0) {
                ampm = "AM";
            }
            if (DungeonQuitController.getInstance().isgameStart == false) {
                if ((ampm.equals("AM") && cal.get(hour) == 8 && cal.get(minute) == 59)) {//Initialize 8:59 daily
                    DungeonQuitController.getInstance().isgameStart = true;
                    System.out.println("■Dungeon Initialization■: " + ampm + " " + cal.get(hour) + "時" + cal.get(minute) + "Minutes initialized.");
                }
            }
        } catch (Exception e) {
            System.out.println("Time initialization error" + e);
        }
    }

    private void GiranTimeCheck(L1PcInstance pc) {
        if (pc.getGirandungeonTime() == 119) {
            new L1Teleport().teleport(pc, 33419, 32810, (short) 4, 0, true);
            pc.sendPackets(new S_SystemMessage("Giran prison dungeon time has passed."));
        }
        pc.setGirandungeonTime(pc.getGirandungeonTime() + 1);
    }

    private void OrenTimeCheck(L1PcInstance pc) {
        if (pc.getOrendungeonTime() == 59) {
            new L1Teleport().teleport(pc, 33419, 32810, (short) 4, 0, true);
            pc.sendPackets(new S_SystemMessage("Dungeon time has passed."));
        }
        pc.setOrendungeonTime(pc.getOrendungeonTime() + 1);

    }

    private void DrageonTimeCheck(L1PcInstance pc) {
        if (pc.getDrageonTime() == 119) {
            new L1Teleport().teleport(pc, 33419, 32810, (short) 4, 0, true);
            pc.sendPackets(new S_SystemMessage("\\aAWarning: \\aG[For]\\aA Dungeon time has passed."));
        }
        pc.setDrageonTime(pc.getDrageonTime() + 1);
    }

    private void SomeTimeCheck(L1PcInstance pc) {
        if (pc.getSomeTime() == 29) {
            new L1Teleport().teleport(pc, 33419, 32810, (short) 4, 0, true);
            pc.sendPackets(new S_SystemMessage("\\aAWarning: \\aG[Fantasy Island]\\aA Dungeon Time has passed."));
        }
        pc.setSomeTime(pc.getSomeTime() + 1);
    }

    private void SoulTimeCheck(L1PcInstance pc) {
        if (pc.getSoulTime() == 29) {
            new L1Teleport().teleport(pc, 33419, 32810, (short) 4, 0, true);
            pc.sendPackets(new S_ChatPacket(pc, "The grave time has passed."));
        }
        pc.setSoulTime(pc.getSoulTime() + 1);
    }

    private void RadungeonTimeCheck(L1PcInstance pc) {
        if (pc.getRadungeonTime() == 119) {
            new L1Teleport().teleport(pc, 33419, 32810, (short) 4, 0, true);
            pc.sendPackets(new S_ChatPacket(pc, "Rastabad cave time has passed."));
        }
        pc.setRadungeonTime(pc.getRadungeonTime() + 1);
    }

    private void newdodungeonTimeCheck(L1PcInstance pc) {
        if (pc.getnewdodungeonTime() == 59) {
            new L1Teleport().teleport(pc, 33419, 32810, (short) 4, 0, true);
            pc.sendPackets(new S_SystemMessage("\\aAWarning: \\aG[Balrog Camp]\\aA Dungeon time has passed."));
        }
        pc.setnewdodungeonTime(pc.getnewdodungeonTime() + 1);
    }

    private void icedungeonTimeCheck(L1PcInstance pc) {
        if (pc.geticedungeonTime() == 29) {
            new L1Teleport().teleport(pc, 33419, 32810, (short) 4, 0, true);
            pc.sendPackets(new S_SystemMessage("\\aAWarning: \\aG[Ice PC]\\aA Dungeon time has passed."));
        }
        pc.seticedungeonTime(pc.geticedungeonTime() + 1);
    }

    private void islanddungeonTimeCheck(L1PcInstance pc) {
        if (pc.getislandTime() == 119) {
            new L1Teleport().teleport(pc, 32585, 32929, (short) 0, 0, true);
            pc.sendPackets(new S_SystemMessage("\\aAWarning: \\aG[Talking Island]\\aA Dungeon time has passed."));
        }
        pc.setislandTime(pc.getislandTime() + 1);
    }
}