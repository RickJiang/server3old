package l1j.server.server.serverpackets;

import l1j.server.server.Opcodes;
import l1j.server.server.model.Instance.L1PcInstance;

public class S_ChatPacket extends ServerBasePacket {

    private static final String _S__1F_NORMALCHATPACK = "[S] S_ChatPacket";
    private byte[] _byte = null;

    public S_ChatPacket(String targetname, String chat, int opcode) {
        writeC(opcode);
        writeC(9);
        writeS("-> (" + targetname + ") " + chat);
    }

    public S_ChatPacket(String targetname, int type, String chat) {
        writeC(Opcodes.S_MESSAGE);
        writeC(type);
        writeS("[" + targetname + "] " + chat);
    }

    // Manager whispers
    public S_ChatPacket(String from, String chat) {
        writeC(Opcodes.S_TELL);
        writeS(from);
        writeS(chat);
    }

    public S_ChatPacket(L1PcInstance pc, String chat) {
        writeC(Opcodes.S_MESSAGE);
        writeC(3);//11
        writeS(chat);
    }

    public S_ChatPacket(String chat) {
        writeC(Opcodes.S_MESSAGE);
        writeC(0x0F);
        writeD(000000000);
        writeS(chat);
    }

    public S_ChatPacket(L1PcInstance pc, String chat, int a, int b, int c) {
        writeC(Opcodes.S_MESSAGE);
        writeC(4);
        writeS(chat);
    }

    public S_ChatPacket(L1PcInstance pc, String chat, int test) {
        writeC(Opcodes.S_SAY);
        writeC(15);
        writeD(pc.getId());
        writeS(chat);
    }

    public S_ChatPacket(String chat, int opcode) {
        writeC(opcode);
        writeC(3);
        writeS(chat);
    }

    public S_ChatPacket(L1PcInstance pc, String chat, int opcode, int type) {
        writeC(opcode);

        switch (type) {
            case 0: // Normal chat
                writeC(type);
                writeD(pc.getId());
                //Battle zone
                if (!pc.isGm() && pc.getMapId() == 5153) {
                    int DuelLine = pc.get_DuelLine();
                    if (DuelLine == 1) {
                        writeS("1回 : " + chat);
                    } else if (DuelLine == 2) {
                        writeS("2回 : " + chat);
                    } else {
                        writeS("Spectator : " + chat);
                    }
                }
                if (pc.is9Militia()) {
                    writeS("[9th Soldier]" + pc.getName() + ": " + chat);
                } else if (pc.is8Militia()) {
                    writeS("[8th Soldier]" + pc.getName() + ": " + chat);
                } else if (pc.is7Militia()) {
                    writeS("[7th Soldier]" + pc.getName() + ": " + chat);
                } else if (pc.is6Militia()) {
                    writeS("[6th Soldier]" + pc.getName() + ": " + chat);
                } else if (pc.is5Militia()) {
                    writeS("[5th Soldier]" + pc.getName() + ": " + chat);
                } else if (pc.is4Militia()) {
                    writeS("[4th Soldier]" + pc.getName() + ": " + chat);
                } else if (pc.is3Militia()) {
                    writeS("[3rd Soldier]" + pc.getName() + ": " + chat);
                } else if (pc.is2Militia()) {
                    writeS("[2nd Soldier]" + pc.getName() + ": " + chat);
                } else if (pc.is1Militia()) {
                    writeS("[1st Soldier]" + pc.getName() + ": " + chat);
                } else if (pc.is1Officer()) {
                    writeS("[1st Officer]" + pc.getName() + ": " + chat);
                } else if (pc.is2Officer()) {
                    writeS("[2nd Officer]" + pc.getName() + ": " + chat);
                } else if (pc.is3Officer()) {
                    writeS("[3rd Officer]" + pc.getName() + ": " + chat);
                } else if (pc.is4Officer()) {
                    writeS("[4th Officer]" + pc.getName() + ": " + chat);
                } else if (pc.is5Officer()) {
                    writeS("[5th Officer]" + pc.getName() + ": " + chat);
                } else if (pc.isGeneral()) {
                    writeS("[General]" + pc.getName() + ": " + chat);
                } else if (pc.isMajorGeneral()) {
                    writeS("[Major]" + pc.getName() + ": " + chat);
                } else if (pc.isCommander()) {
                    writeS("[Commander]" + pc.getName() + ": " + chat);
                } else if (pc.isGeneralCommander()) {
                    writeS("[General Commander]" + pc.getName() + ": " + chat);
                } else {
                    writeS(pc.getName() + ": " + chat);
                }
                break;
            case 2: // 絶叫
                writeC(type);
                if (pc.isInvisble()) {
                    writeD(0);
                } else {
                    writeD(pc.getId());
                }
                writeS("<" + pc.getName() + "> " + chat);
                writeH(pc.getX());
                writeH(pc.getY());
                break;
            case 3:
                writeC(type);
                if (pc.getName().equalsIgnoreCase("メティス") && !pc.getName().equalsIgnoreCase("ほほ笑み彼我") && !pc.getName().equalsIgnoreCase("カシオペア")) {
                    writeS("[******] " + chat);
                }
                if (pc.is9Militia() && !pc.isGm()) {
                    writeS("[9th Soldier][" + pc.getName() + "] " + chat);
                } else if (pc.is8Militia() && !pc.isGm()) {
                    writeS("[8th Soldier][" + pc.getName() + "] " + chat);
                } else if (pc.is7Militia() && !pc.isGm()) {
                    writeS("[7th Soldier][" + pc.getName() + "] " + chat);
                } else if (pc.is6Militia() && !pc.isGm()) {
                    writeS("[6th Soldier][" + pc.getName() + "] " + chat);
                } else if (pc.is5Militia() && !pc.isGm()) {
                    writeS("[5th Soldier][" + pc.getName() + "] " + chat);
                } else if (pc.is4Militia() && !pc.isGm()) {
                    writeS("[4th Soldier][" + pc.getName() + "] " + chat);
                } else if (pc.is3Militia() && !pc.isGm()) {
                    writeS("[3rd Soldier][" + pc.getName() + "] " + chat);
                } else if (pc.is2Militia() && !pc.isGm()) {
                    writeS("[2nd Soldier][" + pc.getName() + "] " + chat);
                } else if (pc.is1Militia() && !pc.isGm()) {
                    writeS("[1st Soldier][" + pc.getName() + "] " + chat);
                } else if (pc.is1Officer() && !pc.isGm()) {
                    writeS("\\fR[1st Officer][" + pc.getName() + "] " + chat);
                } else if (pc.is2Officer() && !pc.isGm()) {
                    writeS("\\fR[2nd Officer][" + pc.getName() + "] " + chat);
                } else if (pc.is3Officer() && !pc.isGm()) {
                    writeS("\\fR[3rd Officer][" + pc.getName() + "] " + chat);
                } else if (pc.is4Officer() && !pc.isGm()) {
                    writeS("\\fR[4th Officer][" + pc.getName() + "] " + chat);
                } else if (pc.is5Officer() && !pc.isGm()) {
                    writeS("\\fR[5th Officer][" + pc.getName() + "] " + chat);
                } else if (pc.isGeneral() && !pc.isGm()) {
                    writeS("\\fR[General][" + pc.getName() + "] " + chat);
                } else if (pc.isMajorGeneral() && !pc.isGm()) {
                    writeS("\\fR[Major][" + pc.getName() + "] " + chat);
                } else if (pc.isCommander() && !pc.isGm()) {
                    writeS("\\fR[司令官][" + pc.getName() + "] " + chat);
                } else if (pc.isGeneralCommander() && !pc.isGm()) {
                    writeS("\\fR[General Commander][" + pc.getName() + "] " + chat);
                } else {
                    writeS("[" + pc.getName() + "] " + chat);
                }
                break;
            case 4: // Clan Chat
                writeC(type);
                if (pc.getAge() == 0) {
                    writeS("{" + pc.getName() + "} " + chat);
                } else {
                    writeS("{" + pc.getName() + "(" + pc.getAge() + ")" + "} " + chat);
                }
                break;
            case 9: // Whisper
                writeC(type);
                writeS("-> (" + pc.getName() + ") " + chat);
                break;
            case 11: // Party Chat
                writeC(type);
                writeS("(" + pc.getName() + ") " + chat);
                break;
            case 12: // Union chat
                writeC(type);
                writeS("[" + pc.getName() + "] " + chat);
                break;
            case 13:
                writeC(4);
                writeS("{{" + pc.getName() + "}} " + chat);
                break;
            case 14: // Chat party
                writeC(type);
                writeD(pc.getId());
                writeS("\\fU(" + pc.getName() + ") " + chat); // #
                break;
            case 15:
                writeC(type);
                writeS("[" + pc.getName() + "] " + chat);
                break;
            case 16: // Whisper
                writeS(pc.getName());
                writeS(chat);
                break;
            case 17: // royal chat +
                writeC(type);
                writeS("{" + pc.getName() + "} " + chat);
                break;
            default:
                break;
        }
    }

    @Override
    public byte[] getContent() {
        if (null == _byte) {
            _byte = _bao.toByteArray();
        }
        return _byte;
    }

    @Override
    public String getType() {
        return _S__1F_NORMALCHATPACK;
    }

}