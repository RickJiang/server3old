/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package l1j.server.server.Controller;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import l1j.server.Config;
import l1j.server.server.datatables.AuctionBoardTable;
import l1j.server.server.datatables.ClanTable;
import l1j.server.server.datatables.HouseTable;
import l1j.server.server.datatables.ItemTable;
import l1j.server.server.model.L1Clan;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1ItemInstance;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.model.item.L1ItemId;
import l1j.server.server.serverpackets.S_ServerMessage;
import l1j.server.server.storage.CharactersItemStorage;
import l1j.server.server.templates.L1AuctionBoard;
import l1j.server.server.templates.L1House;

public class AuctionTimeController implements Runnable {
    public static final int SLEEP_TIME = 60000;
    private static Logger _log = Logger.getLogger(AuctionTimeController.class
            .getName());

    private static AuctionTimeController _instance;

    public static AuctionTimeController getInstance() {
        if (_instance == null) {
            _instance = new AuctionTimeController();
        }
        return _instance;
    }

    @Override
    public void run() {
        try {
            checkAuctionDeadline();
        } catch (Exception e1) {
        }
    }

    public Calendar getRealTime() {
        TimeZone tz = TimeZone.getTimeZone(Config.TIME_ZONE);
        Calendar cal = Calendar.getInstance(tz);
        return cal;
    }

    private void checkAuctionDeadline() {
        AuctionBoardTable boardTable = new AuctionBoardTable();
        for (L1AuctionBoard board : boardTable.getAuctionBoardTableList()) {
            if (board.getDeadline().before(getRealTime())) {
                endAuction(board);
            }
        }
    }

    private void endAuction(L1AuctionBoard board) {
        int houseId = board.getHouseId();
        int price = board.getPrice();
        int oldOwnerId = board.getOldOwnerId();
        String bidder = board.getBidder();
        int bidderId = board.getBidderId();

        if (oldOwnerId != 0 && bidderId != 0) { // The previous owner has the highest bidder
            L1PcInstance oldOwnerPc = (L1PcInstance) L1World.getInstance()
                    .findObject(oldOwnerId);
            int payPrice = (int) (price * 0.9);
            if (oldOwnerPc != null) { // Previous owner is online
                oldOwnerPc.getInventory().storeItem(L1ItemId.ADENA, payPrice);
                // The house you owned was awarded the final price of %1 Adena. ％n
                // The remaining amount will be Adena %0, excluding 10%% of the fee.％n Thank you very much. ％n％n
                oldOwnerPc.sendPackets(new S_ServerMessage(527, String.valueOf(payPrice)));
            } else { // Previous owner offline
                L1ItemInstance item = ItemTable.getInstance().createItem(L1ItemId.ADENA);
                item.setCount(payPrice);
                try {
                    CharactersItemStorage storage = CharactersItemStorage.create();
                    storage.storeItem(oldOwnerId, item);
                } catch (Exception e) {
                    _log.log(Level.SEVERE, e.getLocalizedMessage(), e);
                }
            }

            L1PcInstance bidderPc = (L1PcInstance) L1World.getInstance().findObject(bidderId);
            if (bidderPc != null) { //The highest bidder is online.
                // Congratulations to you. %n The auction you participated in was bid at the price of the final price %0 Adena. %n
                // The house you bought can be used immediately. %n Thank you. %n %n
                bidderPc.sendPackets(new S_ServerMessage(524, String.valueOf(price), bidder));
            }
            deleteHouseInfo(houseId);
            setHouseInfo(houseId, bidderId);
            deleteNote(houseId);
        } else if (oldOwnerId == 0 && bidderId != 0) { // no previous owner/highest bidder available
            L1PcInstance bidderPc = (L1PcInstance) L1World.getInstance().findObject(bidderId);
            if (bidderPc != null) { // highest bidder is online.
                // congratulations. %n The auction you participated in was awarded at the final price %0 Adena's price. %n
                // The house you bought can be used immediately. %n Thank you. %n %n
                bidderPc.sendPackets(new S_ServerMessage(524, String
                        .valueOf(price), bidder));
            }
            setHouseInfo(houseId, bidderId);
            deleteNote(houseId);
        } else if (oldOwnerId != 0 && bidderId == 0) { // no previous owner or winner
            L1PcInstance oldOwnerPc = (L1PcInstance) L1World.getInstance()
                    .findObject(oldOwnerId);
            if (oldOwnerPc != null) { // previous owner is online
                // it was removed because you did not see that the auction you applied stated that you were paying more than the amount presented in the auction period. %n
                // Therefore, we will inform you that ownership has been returned to you. %n Thank you.％n％n
                oldOwnerPc.sendPackets(new S_ServerMessage(528));
            }
            deleteNote(houseId);
        } else if (oldOwnerId == 0 && bidderId == 0) { // no previous owner · no winning bidder
            // set the deadline 5 days later and put it on the auction again
            Calendar cal = getRealTime();
            cal.add(Calendar.DATE, 1);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            board.setDeadline(cal);
            AuctionBoardTable boardTable = new AuctionBoardTable();
            boardTable.updateAuctionBoard(board);
        }
    }

    /**
     * Remove the previous owner's house
     *
     * @param houseId
     * @return
     */
    private void deleteHouseInfo(int houseId) {
        for (L1Clan clan : L1World.getInstance().getAllClans()) {
            if (clan.getHouseId() == houseId) {
                clan.setHouseId(0);
                ClanTable.getInstance().updateClan(clan);
            }
        }
    }

    /**
     * Set up a house for the highest bidder
     *
     * @param houseId bidderId
     * @return
     */
    private void setHouseInfo(int houseId, int bidderId) {
        for (L1Clan clan : L1World.getInstance().getAllClans()) {
            if (clan.getLeaderId() == bidderId) {
                clan.setHouseId(houseId);
                ClanTable.getInstance().updateClan(clan);
                break;
            }
        }
    }

    /**
     * Set the auction status of the hideout to off and remove it from the auction board
     *
     * @param houseId
     * @return
     */
    private void deleteNote(int houseId) {
        // Set the status of the house to off
        L1House house = HouseTable.getInstance().getHouseTable(houseId);
        house.setOnSale(false);
        Calendar cal = getRealTime();
        cal.add(Calendar.DATE, Config.HOUSE_TAX_INTERVAL);
        cal.set(Calendar.MINUTE, 0); // Minutes, seconds truncated
        cal.set(Calendar.SECOND, 0);
        house.setTaxDeadline(cal);
        HouseTable.getInstance().updateHouse(house);

        // remove from the auction board
        AuctionBoardTable boardTable = new AuctionBoardTable();
        boardTable.deleteAuctionBoard(houseId);
    }

}
