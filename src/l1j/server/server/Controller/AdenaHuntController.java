package l1j.server.server.Controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import l1j.server.Config;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.L1World;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_PacketBox;
import l1j.server.server.serverpackets.S_SystemMessage;

public class AdenaHuntController extends Thread {

    private static AdenaHuntController _instance;

    private boolean _AdenaHuntStart;

    public boolean getAdenaHuntStart() {
        return _AdenaHuntStart;
    }

    public void setAdenaHuntStart(boolean AdenaHunt) {
        _AdenaHuntStart = AdenaHunt;
    }

    private static long sTime = 0;

    public boolean isGmOpen4 = false;

    private String NowTime = "";

    private static final int ADENTIME = Config.ADEN_HUNTING_TIME;

    private static final SimpleDateFormat s = new SimpleDateFormat("HH", Locale.KOREA);

    private static final SimpleDateFormat ss = new SimpleDateFormat("MM-dd HH:mm", Locale.KOREA);

    public static AdenaHuntController getInstance() {
        if (_instance == null) {
            _instance = new AdenaHuntController();
        }
        return _instance;
    }

    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep(1000);
                /** Open **/
                if (!isOpen6() && !isGmOpen4)
                    continue;
                if (L1World.getInstance().getAllPlayers().size() <= 0)
                    continue;

                isGmOpen4 = false;

                L1World.getInstance().broadcastPacketToAll(new S_PacketBox(S_PacketBox.GREEN_MESSAGE, "[Announcements] Aden Hunting Ground is now open."));
                L1World.getInstance().broadcastServerMessage("\\aH............ Aden Hunting Ground is now open. ............");
                L1World.getInstance().broadcastServerMessage("\\aH............  Enjoy your hunt!  ............");

                setAdenaHuntStart(true);

                Thread.sleep(3800000L); //About 60 minutes

                TelePort3();
                Thread.sleep(5000L);
                TelePort4();

                /** end**/
                End();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Bring open time
     *
     * @return (Strind) Open time(MM-dd HH:mm)
     */
    public String AdenOpen() {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(sTime);
        return ss.format(c.getTime());
    }

    /**
     * Make sure the map is open
     *
     * @return (boolean) When/If open; True = open; False = closed.
     */
    private boolean isOpen6() {
        NowTime = getTime();
        if ((Integer.parseInt(NowTime) % ADENTIME) == 0) return true;
        return false;
    }

    //Add to the demon king's territory source reference.
    public boolean isOpen7() {
        NowTime = getTime();
        if ((Integer.parseInt(NowTime) % ADENTIME) >= 2
                && (Integer.parseInt(NowTime) % ADENTIME) <= 8)
            return true;
        return false;
    }

    /**
     * Actually bring the current time
     *
     * @return (String)Current time（HH：mm）
     */
    private String getTime() {
        return s.format(Calendar.getInstance().getTime());
    }

    /**
     * Teleport to Guillian Village (Mainland?)
     **/
    private void TelePort3() {
        for (L1PcInstance c : L1World.getInstance().getAllPlayers()) {
            switch (c.getMap().getId()) {
                case 701: //Aden Hunting Ground
                    c.stopHpRegenerationByDoll();
                    c.stopMpRegenerationByDoll();
                    new L1Teleport().teleport(c, 32617, 32773, (short) 4, c.getHeading(), true);
                    c.sendPackets(new S_SystemMessage("The Aden hunting ground has ended."));
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Teleport to Guillian Village (Mainland?)
     **/
    private void TelePort4() {
        for (L1PcInstance c : L1World.getInstance().getAllPlayers()) {
            switch (c.getMap().getId()) {
                case 701: //Aden Hunting Ground
                    c.stopHpRegenerationByDoll();
                    c.stopMpRegenerationByDoll();
                    new L1Teleport().teleport(c, 32617, 32773, (short) 4, c.getHeading(), true);
                    c.sendPackets(new S_SystemMessage("The Aden hunting ground has ended."));
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * End
     **/
    public void End() {
        L1World.getInstance().broadcastServerMessage("\\fSThe Aden hunting ground has ended.");

        L1World.getInstance().broadcastServerMessage("\\fSAden Hunting ground is [" + Config.ADEN_HUNTING_TIME + "time] interval.");
        setAdenaHuntStart(false);
    }
}

